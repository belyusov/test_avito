package ru.belyusov.avito.service;

import ru.belyusov.avito.model.User;

import java.util.List;

/**
 * Created by max on 17.11.16.
 */
public interface UserService {
    public void addUser(User contact);

    public List<User> listUser();

    public void removeUser(long id);
}
