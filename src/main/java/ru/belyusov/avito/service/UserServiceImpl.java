package ru.belyusov.avito.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.belyusov.avito.dao.UserDao;
import ru.belyusov.avito.model.Order;
import ru.belyusov.avito.model.User;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by max on 17.11.16.
 */

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {
    private Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
    @Autowired
    private UserDao userDao;

    public UserServiceImpl(UserDao userDao) {
        this.userDao=userDao;
    }


    public Order getOrderByID(long id) {
        Order order = new Order();
        System.out.println("Получаем order " + id);
        return order;
    }


    @Transactional(readOnly = false)
    public void addUser(User user) {
        log.debug("add user"+user.getId());
        userDao.addUser(user);

    }

    @Transactional(readOnly = true)
    public List<User> listUser() {
        return userDao.listUser();
    }

    @Transactional
    public void removeUser(long id) {
        userDao.removeUser(id);

    }

}
