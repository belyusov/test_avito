package ru.belyusov.avito.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.belyusov.avito.model.User;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.TransactionManager;
import java.util.List;

/**
 * Created by max on 17.11.16.
 */

@Repository("userDao")
public class UserDAOImpl implements UserDao {

/*
    @PersistenceContext
    private EntityManager manager;
*/
    @Autowired
    SessionFactory sessionFactory;

    public void addUser(User user) {
       // manager.persist(user);
        sessionFactory.getCurrentSession().persist(user);
    }

    @SuppressWarnings("unchecked")
    public List<User> listUser() {
        //List<User> users = manager.createQuery("Select a From User a", User.class).getResultList();
        List<User> users = sessionFactory.getCurrentSession().createQuery("Select a From User a").list();
        return users;
    }

    public void removeUser(long id) {
/*
        User user = (User) manager.find(User.class, id);
        if (user != null) {
            manager.remove(user);
        }
*/
    }
}
