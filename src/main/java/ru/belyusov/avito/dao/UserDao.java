package ru.belyusov.avito.dao;

import ru.belyusov.avito.model.User;

import java.util.List;

/**
 * Created by max on 17.11.16.
 */
public interface UserDao {
    public void addUser(User user);

    public List<User> listUser();

    public void removeUser(long id);
}
