package ru.belyusov.avito.utils;


import org.apache.commons.beanutils.PropertyUtilsBean;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by max on 17.11.16.
 */
public class TransformerUtils {
    private static PropertyUtilsBean propertyUtilsBean = new PropertyUtilsBean();

    public static <T>T transform(Object from, Class<T> clazz) {
        Object toInstance = null;
        try {
            toInstance = clazz.newInstance();
            propertyUtilsBean.copyProperties(toInstance, from);
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return (T) toInstance;
    }

    public static <T> List<T> transform(List from, Class<T> clazz) {

        List<T> res = new ArrayList<>();

        for(Object o: from) {
            res.add(transform(o, clazz));
        }

        return res;
    }

}
