package ru.belyusov.avito.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.belyusov.avito.model.User;
import ru.belyusov.avito.service.UserService;

import java.util.List;

/**
 * Created by max on 17.11.16.
 */

@RestController
@RequestMapping("/")
public class MainController {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public List<User> getOrder(Model model) {
        System.out.println("mainController....");
        userService.addUser(new User(800.0));
        //userService.removeUser(2);

        List<User> users = userService.listUser();
        model.addAttribute("users", users);
/*
        for(User u : users){
            System.out.println(u.getId()+ "=" + u.getBalance());
        }
*/

        //return "main";
        return users;
    }

}
