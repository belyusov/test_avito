package ru.belyusov.avito.model;

/**
 * Created by max on 17.11.16.
 */
public enum OrderStatus {
    NEW,
    SHIPMENT,
    DONE
}
