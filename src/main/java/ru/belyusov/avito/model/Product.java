package ru.belyusov.avito.model;

import javax.persistence.*;

/**
 * Created by max on 17.11.16.
 */
@Entity
@Table(name = "PRODUCT")
public class Product {
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    long id;
    @Column(name = "ARTICLE", nullable = false)
    String article;
    @Column(name = "NAME", length = 30, nullable = false)
    String name;
    @Column(name = "PRICE",  length = 250, nullable = false)
    Double price;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
