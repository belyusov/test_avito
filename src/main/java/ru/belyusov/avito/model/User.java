package ru.belyusov.avito.model;

//import javax.persistence.*;
import javax.persistence.*;
import java.util.List;

/**
 * Created by max on 17.11.16.
 */
@Entity
@Table(name = "USERS")
public class User {
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;
    @Column(name = "BALANCE", nullable = false)
    private Double balance;


    //@ElementCollection(targetClass=Order.class)
//    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
//    private List<Order> orderList;

    public User() {
    }

    public User(Double balance) {
        this.balance = balance;
    }

    public User(long id, Double balance) {
        this.id = id;
        this.balance = balance;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

/*
    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;

    }
    */
}
