package ru.belyusov.avito.model.dto;

/**
 * Created by max on 18.11.16.
 */
public class UserDto {
    Double balance;

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }
}
