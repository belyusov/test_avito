package ru.belyusov.avito.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by max on 17.11.16.
 */

@Entity
@Table(name = "ORDERS")
public class Order {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
    @Column(name = "ORDER_NAME", length = 20)
    private String orderNumber;
    @Column(name = "DATE")
    private Date date;
    @Column(name = "STATUS")
    private OrderStatus status;

    @OneToMany
    private List<Product> products;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User user;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

/*
    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
*/

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
